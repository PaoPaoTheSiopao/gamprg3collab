﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attributes : MonoBehaviour
{
    public float power;
    public float modPower;
    public float precision;
    public float modPrecision;
    public float toughness;
    public float modToughness;
    public float vitality;
    public float modVitality;

    // Start is called before the first frame update
    void Start()
    {
        precision = 1000;
        toughness = 25;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
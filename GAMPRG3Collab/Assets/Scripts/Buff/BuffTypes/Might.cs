﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Might : Buff
{
    [SerializeField]
    private GameObject playerReference;

    void Awake()
    {
        name = "Might";
        stack = StackType.IntensityStack;
        curStack = 1;
        maxStack = 10;
        withDur = false;

        //GameObject might = Instantiate(gameObject, transform.position, transform.rotation);

        playerReference = FindObjectOfType<Player>().gameObject;

        SetParameters(playerReference, playerReference, 10.0f, 5.0f);

        transform.SetParent(playerReference.transform);

        receiver = playerReference.gameObject.GetComponent<BuffReceiver>();
    }

    private void BuffEffect()
    {
        Attributes playerAttributes = playerReference.GetComponent<Attributes>();

        playerAttributes.modPower = playerAttributes.power + 100.0f;
    }

    protected override void Effect()
    {
        for(int i = 0; i < condStack.Count; i++)
        {
            BuffEffect();
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Buff
{
    [SerializeField]
    private GameObject playerReference;
    void Awake()
    {
        name = "Poison";
        stack = StackType.IntensityStack;
        curStack = 1;
        maxStack = 5;
        withDur = true;

        playerReference = FindObjectOfType<Player>().gameObject;

        SetParameters(playerReference, playerReference, 10.0f, 5.0f);

        receiver = playerReference.gameObject.GetComponent<BuffReceiver>();
    }

    private void BuffEffect()
    {
        //Debug.Log("Poisoned");

        Attributes playerAttributes = playerReference.GetComponent<Attributes>();

        float healthReference = playerReference.GetComponent<Player>().health;

        playerReference.GetComponent<Player>().health -= (playerReference.GetComponent<Player>().health * 0.02f);

        if(playerReference.GetComponent<Player>().health <= 0)
        {
            playerReference.GetComponent<Player>().health = 0;
        }

        //Debug.Log(healthReference);
    }

    protected override void Effect()
    {
        for(int i = 0; i < condStack.Count; i++)
        {
            BuffEffect();
        }
    }
}

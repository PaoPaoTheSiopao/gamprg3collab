﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Regeneration : Buff
{
    [SerializeField]
    private GameObject playerReference;
    void Awake()
    {
        name = "Regeneration";
        stack = StackType.DurationStack;
        curStack = 1;
        maxStack = 1;
        withDur = true;

        playerReference = FindObjectOfType<Player>().gameObject;

        SetParameters(playerReference, playerReference, 10.0f, 5.0f);

        receiver = playerReference.gameObject.GetComponent<BuffReceiver>();
    }

    private void BuffEffect()
    {
        Attributes playerAttributes = playerReference.GetComponent<Attributes>();

        float healthReference = playerReference.GetComponent<Player>().health;

        playerReference.GetComponent<Player>().health += (playerReference.GetComponent<Player>().health * 0.05f);

        if(playerReference.GetComponent<Player>().health >= playerReference.GetComponent<Player>().maxHealth)
        {
            playerReference.GetComponent<Player>().health = 0;
        }
    }

    protected override void Effect()
    {
        BuffEffect();
    }
}
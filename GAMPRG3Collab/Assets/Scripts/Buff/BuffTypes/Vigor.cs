﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vigor : Buff
{
    [SerializeField]
    private GameObject playerReference;
    void Awake()
    {
        name = "Vigor";
        stack = StackType.DurationStack;
        curStack = 1;
        maxStack = 1;
        withDur = false;

        playerReference = FindObjectOfType<Player>().gameObject;

        SetParameters(playerReference, playerReference, 10.0f, 5.0f);

        receiver = playerReference.gameObject.GetComponent<BuffReceiver>();
    }

    private void BuffEffect()
    {
        Attributes playerAttributes = playerReference.GetComponent<Attributes>();

        playerAttributes.modVitality = playerAttributes.vitality + (playerAttributes.vitality * 0.5f);
    }

    protected override void Effect()
    {
        BuffEffect();
    }
}
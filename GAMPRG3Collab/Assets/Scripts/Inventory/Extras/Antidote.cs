﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Antidote")]
public class Antidote : ItemEffect
{
    public override void ApplyEffect()
    {
        GameObject source = GameObject.FindGameObjectWithTag("Player");
        BuffReceiver r = source.GetComponent<BuffReceiver>();
        for(int i = 0; i<r.buffs.Count;i++)
        {
            if(r.buffs[i].GetComponent<Buff>().name == "Poison")
            {
                r.buffs[i].GetComponent<Buff>().RemoveBuff();
            }
        }
    }
}

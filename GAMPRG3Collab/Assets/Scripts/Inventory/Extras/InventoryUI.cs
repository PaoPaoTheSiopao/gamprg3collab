﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUI : MonoBehaviour
{
    public Transform itemsParent;


    public Inventory inventory;

    ItemInventorySlots[] slots;
    // Start is called before the first frame update
    void Start()
    {
        inventory.invUI = this;
        slots = itemsParent.GetComponentsInChildren<ItemInventorySlots>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void UpdateUI()
    {
        for (int i = 0; i < inventory.inventorySlots.Count; i++)
        {
            if (inventory.inventorySlots[i].slotStackCount > 0)
            {
                slots[i].AddItem(inventory.inventorySlots[i].currentItem, inventory.inventorySlots[i], inventory);
                Debug.Log("additem");
            }
            else
            {
                Debug.Log("clear");
                slots[i].ClearItem();
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInventorySlots : MonoBehaviour
{
    public Image icon;
    public GameObject remButton;
    public GameObject stackDigit;
    public Item item;
    public InventorySlot itemslot;
    public Inventory inventory;

    public void AddItem(Item newItem,InventorySlot newslot,Inventory newInventory)
    {
        item = newItem;
        itemslot = newslot;
        inventory = newInventory;

        icon.sprite = item.icon;
        icon.enabled = true;
        remButton.SetActive(true);
        stackDigit.SetActive(true);
        stackDigit.GetComponent<Text>().text = newslot.slotStackCount.ToString();
    }

    public void ClearItem()
    {
        item = null;
        itemslot = null;
        inventory = null;

        icon.sprite = null;
        icon.enabled = false;

        remButton.SetActive(false);
        stackDigit.SetActive(false);
    }

    public void UseItem()
    {
        //item.itemEffect.ApplyEffect();
        inventory.UseItemP(item);
        stackDigit.GetComponent<Text>().text = itemslot.slotStackCount.ToString();
    }
}

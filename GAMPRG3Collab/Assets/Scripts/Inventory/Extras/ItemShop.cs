﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemShop : MonoBehaviour
{
    public Inventory inventory;
    
    public List<Item> items;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            BuyEmblem();
        }
    }

    public void BuyEmblem()
    {
        InventorySlot slot = new InventorySlot();
        Item emblem = items[2];

        slot.currentItem = emblem;
        slot.slotStackCount = 0;

        inventory.CheckInventory(emblem);
    }

    public void BuyRejuvenatingPotion()
    {
        InventorySlot slot = new InventorySlot();

        Item rejPot = items[1];

        slot.currentItem = rejPot;
        slot.slotStackCount = 0;

        inventory.CheckInventory(rejPot);
    }

    public void BuyWeirdMushroom()
    {
        InventorySlot slot = new InventorySlot();

        Item weirdMush = items[3];

        slot.currentItem = weirdMush;
        slot.slotStackCount = 0;

        inventory.CheckInventory(weirdMush);
    }

    public void BuyAntidote()
    {
        InventorySlot slot = new InventorySlot();

        Item antidote = items[0];

        slot.currentItem = antidote;
        slot.slotStackCount = 0;

        inventory.CheckInventory(antidote);
    }
}

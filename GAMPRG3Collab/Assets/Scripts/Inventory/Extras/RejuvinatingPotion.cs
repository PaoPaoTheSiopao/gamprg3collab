﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/RejuvinatingPotion")]
public class RejuvinatingPotion : ItemEffect
{
    public override void ApplyEffect()
    {
        GameObject source = GameObject.FindGameObjectWithTag("Player");
        GameObject b = Instantiate(buffEff, source.transform.position, source.transform.rotation);
        b.transform.SetParent(source.transform);
        Buff buf = b.GetComponent<Buff>();
        //Source of the buff, the target, duration, condition 
        buf.SetParameters(source, source, 15, 5);
    }
}


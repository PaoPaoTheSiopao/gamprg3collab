﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopKeeperItem : MonoBehaviour
{
    public Item item;

    public Image icon;
    public Inventory inventory;
    private void Start()
    {
        icon.sprite = item.icon;
    }

    public void BuyItem()
    {
        inventory.CheckInventory(item);
    }
}

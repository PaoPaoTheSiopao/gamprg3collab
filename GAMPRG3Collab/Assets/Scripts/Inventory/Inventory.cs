﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public int inventoryMaxSize = 12;

    public List<InventorySlot> inventorySlots = new List<InventorySlot>();
    public Dictionary<string, InventorySlot> dictionary = new Dictionary<string, InventorySlot>();
    //
    public InventoryUI invUI;
    public Sprite testIm;
    public GameObject source;
    public GameObject target;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        OnKeyPressAddItems();
    }

    // Add Items on Key Press (For Testing Only)
    private void OnKeyPressAddItems()
    {
        // Add Iron Sword
        if(Input.GetKeyDown(KeyCode.A))
        {
            InventorySlot slot = new InventorySlot();

            Item newItem = new Item()
            {
                name = "Iron Sword",
                itemName = "Iron Sword",
                isUsable = false,
                stackCount = 0,
                isStackable = true,
                maxStackCount = 5,
                icon = testIm
            };

            slot.currentItem = newItem;
            slot.slotStackCount = 0;

            CheckInventory(newItem);
        }

        // Add Potion
        if(Input.GetKeyDown(KeyCode.S))
        {
            InventorySlot slot = new InventorySlot();

            Item newItem = new Item()
            {
                name = "Potion",
                itemName = "Potion",
                isUsable = true,
                stackCount = 0,
                isStackable = true,
                maxStackCount = 5
            };

            slot.currentItem = newItem;
            slot.slotStackCount = 0;

            CheckInventory(newItem);
        }

        // Add Antidote
        if(Input.GetKeyDown(KeyCode.D))
        {
            InventorySlot slot = new InventorySlot();

            Item newItem = new Item()
            {
                name = "Antidote",
                itemName = "Antidote",
                isUsable = true,
                stackCount = 0,
                isStackable = true,
                maxStackCount = 5
            };

            slot.currentItem = newItem;
            slot.slotStackCount = 0;

            CheckInventory(newItem);
        }

        // Add Leather Armor
        if(Input.GetKeyDown(KeyCode.F))
        {
            InventorySlot slot = new InventorySlot();

            Item newItem = new Item()
            {
                name = "Leather Armor",
                itemName = "Leather Armor",
                isUsable = false,
                stackCount = 0,
                isStackable = false,
                maxStackCount = 0
            };

            slot.currentItem = newItem;
            slot.slotStackCount = 0;

            CheckInventory(newItem);
        }
    }

    //Made this public;
    public void CheckInventory(Item newItem)
    {
        // If inventory is not full.
        if (inventorySlots.Count < inventoryMaxSize)
        {
            AddItemToSlots(newItem);
        }

        // Check if inventory is full
        else if (inventorySlots.Count >= inventoryMaxSize)
        {
            return;
        }
    }

    private void AddItemToSlots(Item newItem)
    {
        InventorySlot inventorySlot = new InventorySlot();

        if (dictionary.ContainsKey(newItem.itemName) == true)
        {
            inventorySlot = dictionary[newItem.itemName];

            if (inventorySlot.slotStackCount < newItem.maxStackCount)
            {
                inventorySlot.slotStackCount++;
            }

            else
            {
                if (inventorySlots.Count < inventoryMaxSize)
                {
                    InventorySlot newInventorySlot = new InventorySlot();

                    dictionary.Remove(newItem.itemName);

                    AddItemToList(newInventorySlot, newItem);
                }

                else
                {
                    return;
                }
            }
        }

        else
        {
            AddItemToList(inventorySlot, newItem);
            //
            inventorySlot.slotStackCount++;
        }
        //
        invUI.UpdateUI();
    }

    public void AddItemToList(InventorySlot slot, Item newItem)
    {
        slot.currentItem = newItem;

        inventorySlots.Add(slot);

        dictionary.Add(slot.currentItem.name, slot);
    }

    private Item GetItem(int index)
    {
        if (index > 0)
        {
            return inventorySlots[index].currentItem;
        }

        else
        {
            return null;
        }
    }
    private void UseItem(int index)
    {
        Item tempItem = GetItem(index);

        if(tempItem != null && tempItem.isUsable == true)
        {
            tempItem.itemEffect.ApplyEffect();

            RemoveItem(tempItem);
        }
    }
    //
    public void UseItemP(Item item)
    {
        Item tempItem = item;

        if (tempItem != null && tempItem.isUsable == true)
        {
            tempItem.itemEffect.ApplyEffect();

            RemoveItem(tempItem);
        }
    }

    private void RemoveItem(Item selectedItem)
    {
        if (dictionary.ContainsKey(selectedItem.name) == true)
        {
            InventorySlot tempSlot = dictionary[selectedItem.itemName];

            tempSlot.slotStackCount--;
            //
            invUI.UpdateUI();
            //

            if (tempSlot.slotStackCount <= 0)
            {
                inventorySlots.Remove(tempSlot);
                dictionary.Remove(selectedItem.itemName);

                InventorySlot temp = inventorySlots.Find(x => x.currentItem == selectedItem);

                if (temp != null)
                {
                    dictionary.Add(temp.currentItem.itemName, temp);
                }

                else
                {
                    return;
                }
            }
        }

        
    }
}
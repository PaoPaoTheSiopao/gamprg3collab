﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/Item", order = 1)]
public class Item : ScriptableObject
{
    public string itemName;
    public bool isUsable;
    public int stackCount;
    public bool isStackable;
    public int maxStackCount;
    public ItemEffect itemEffect;
    //
    public Sprite icon;

}
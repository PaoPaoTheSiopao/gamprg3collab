﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemEffect : ScriptableObject
{
    public GameObject buffEff;
    public abstract void ApplyEffect();
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private BuffReceiver receiver;

    [SerializeField]
    private Attributes attributes;

    public float damage;
    public float criticalChance;
    public float baseArmor;
    public float armor;
    public float baseHealth = 50;
    public float health;
    public float currentHealth;
    public float maxHealth;

    public float weaponStrength;
    

    void Awake()
    {

    }
    // Start is called before the first frame update
    void Start()
    {
        attributes = GetComponent<Attributes>();
        receiver = GetComponent<BuffReceiver>();

        currentHealth = health;
        maxHealth = baseHealth + (attributes.vitality * 10);

        UpdateHealth();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateArmor();
        UpdateCriticalChance();
        //UpdateHealth();
        UpdateDamage();
    }

    void UpdateHealth()
    {
        health = baseHealth + (attributes.vitality * 10);
        currentHealth = health;
    }
    void UpdateArmor()
    {
        armor = baseArmor + attributes.toughness;
    }
    void UpdateDamage()
    {
        damage = weaponStrength * attributes.power;
    }
    void UpdateCriticalChance()
    {
        criticalChance = 5 +((attributes.precision - 1000) / 21);
    }
}